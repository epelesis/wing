package com.nedellis

import scalatags.Text.all._
import scalatags.Text.tags2.{title, section}
import cask.{Logger, Routes}
import scalatags.Text

object App extends Routes {
  val HEAD: Text.TypedTag[String] = head(
    meta(charset := "utf-8"),
    meta(name := "viewport", content := "width=device-width, initial-scale=1"),
    link(rel := "stylesheet", href := "https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css"),
    title("Ned Ellis")
  )

  def renderDocument(body: Text.TypedTag[String]) =
    doctype("html")(
      html(lang := "en")(
        HEAD,
        body
      )
    )

  def renderBody(sections: Text.TypedTag[String]*) = body(sections: _*)

  def renderContainer(content: Text.TypedTag[String]*) = section(cls := "section")(
    div(cls := "container is-fluid")(content: _*))

  @cask.get("/")
  def homePage() = renderDocument {
    renderBody(
      renderContainer(
        h1(cls := "title")("Ned Ellis"),
        p(cls := "subtitle")("Welcome to my chunk of the IP address space")
      ),
      renderContainer(
        h3(cls := "title is-3")("About"),
        p(
          """
            | Hello! I am Ned Ellis, and just recently graduated from the University of Illinois Urbana-Champaign with a
            | B.S. in Computer Engineering. I currently work for Databricks as a Software Engineer on the
            | Compute Fabric team.
            |""".trim.stripMargin),
        br(),
        p(
          """
            | I am paticularly interested in the design and implementation of distributed systems, although in the
            | past I have produced my own programming language, wrote a Linux kernel and designed a RISC-V CPU core.
            |""".trim.stripMargin),
        br(),
        p(
          """
            | While this website can be a good first approximation, sometimes it can be hard to express who you are
            | with articles alone. Try to get to know me in person too!
            |""".stripMargin
        )
      ),
      renderContainer(
        h3(cls := "title is-3")("Links"),
        ul(
          li(a(href := "https://www.linkedin.com/in/epellis/")("LinkedIn")),
          li(a(href := "https://github.com/epellis/")("Github")),
        )
      )
    )
  }

  @cask.decorators.compress
  @cask.staticResources("/images/")
  def staticResourceRoutes() = "images"

  initialize()
}
