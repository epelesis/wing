package com.nedellis.components

import scalatags.Text
import scalatags.Text.all._

case class Image(name: String) extends Component {
  override def render(): Text.TypedTag[String] = a(href := name)(img(src := ""))
}
