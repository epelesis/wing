package com.nedellis.components

import scalatags.Text

trait Component {
  def render(): Text.TypedTag[String]
}
