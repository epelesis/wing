lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.nedellis",
      scalaVersion := "2.13.3"
    )),
    name := "sanvaad"
  )

libraryDependencies ++= Seq(
  "com.lihaoyi" %% "cask" % "0.7.8",
  "com.lihaoyi" %% "scalatags" % "0.8.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % Test
